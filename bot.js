const Discord = require('discord.js');
const chalk = require('chalk');
const sql = require('sqlite');
const fs = require('fs');
const moment = require('moment');
const black = require('./config/blacklist.json');
const image = require('./images/images.json');
const config = require('./config/settings.json');
const ball = require('./fun/8ball.json').ball;
const h = require('./config/help.json');
const ddif = require('return-deep-diff');
const mystery = require('./fun/box.json');
const client = new Discord.Client();
sql.open('./score.sqlite');
client.login(config.token);

//Client checkup
client.on("ready", () => {
  console.log(chalk.bgGreen('Lord Hackysack is Online!!!'));
  console.log(`${chalk.cyan('===============Client Stats===============\n')}${chalk.blue('Client Stats:\n')}Guilds: ${client.guilds.size}\nUsers: ${client.users.size}\nChannels: ${client.channels.size}\nReady At: ${moment(client.readyAt).format('MMMM Do YYYY, h:mm:ss a')} ${chalk.cyan('\n============================================')}`);
  var games = [
    `${client.guilds.size} guilds`,
    `${client.users.size} users`,
    `${client.emojis.size} emojis`,
    `${client.channels.size} channels`
  ]
  client.user.setGame("$help | " + client.guilds.size + " servers!");
    setInterval(() => {
      client.user.setGame(`$help | ${games[Math.floor(Math.random() * games.length)]}`);
    }, 150000);
});
client.on("disconnect", () => {
  console.log(`The Bot has been disconnected: ${moment().format('lll')}`)
});
client.on("reconnecting", () => {
  console.log(`The Bot has been Reconnected: ${moment().format('lll')}`)
});

//Functions
function commandIs(str, msg){
    return msg.content.toLowerCase().startsWith("$" + str)
}
function pluck(array){
  return array.map(function(item) {return item["name"]; });
}
function hasRole(mem, roles) {
    if(pluck(mem.roles).includes(roles)){
        return true;
    } else{
        return false;
    }
}
function clean(text){
    if(typeof(text) === "string")
        return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
    else
        return text;
};

var owner = config.ownerid;
/////////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
// Commands
client.on('message', message => {
  if(message.author.bot) return;
  var args = message.content.split(/[ ]+/);
  if(black.ids.includes(message.author.id)) return;
  if(message.channel.type === 'dm'){
    if(message.author.id === owner) return;
    client.users.get(owner).send(`The Bot has been messaged by ${message.author.username}#${message.author.discriminator}\n\`\`\`${message.content}\`\`\``);
  };

  sql.get(`SELECT * FROM scores WHERE userId ='${message.author.id}'`).then(row => {
    if(!row) {
      sql.run('INSERT INTO scores (userId, points, level) VALUES (?, ?, ?)', [message.author.id, 1, 0]);
    } else {
      let curLevel = Math.floor(0.1 * Math.sqrt(row.points + 1));
      if (curLevel > row.level){
        row.level = curLevel;
        sql.run(`UPDATE scores SET points = ${row.points + 1}, level = ${row.level} WHERE userId = ${message.author.id}`);
        message.reply(`You've leveled up\n **Level:** ${curLevel}`);
      }
      sql.run(`UPDATE scores SET points = ${row.points + 1} WHERE userId = ${message.author.id}`);
    };
      }).catch(() => {
        console.error;
          sql.run('CREATE TABLE IF NOT EXISTS scores (userId TEXT, points INTEGER, level INTEGER)').then(() => {
          sql.run('INSTERT INTO scores (userId, points, level) VALUES (?, ?, ?)', [nessage.author.id, 1, 0]);
    });
  });



  //Actions
  if(message.content === 'ayy' || message.content === 'Ayy'){
    message.channel.send("Lmao")
  }
  if(message.isMentioned(client.user)){
    var response = "I'm not paying for cleverbot api 😒";
    message.reply(response);
  }
  if(message.content.toLowerCase() === 'f'){
    message.channel.send('*pays respects*')
  }


  //General Commands
  if(commandIs("hello", message)){
      message.channel.send('`print("hello world, ' + message.author.username + '")`')
  }
  if(commandIs("say", message)){
      if(message.member.hasPermission("MANAGE_MESSAGES")){
      if(args.length === 1){
          message.channel.send("You did not define an Argument.\nUsage: `$say 'msg'`");
      } else {
          message.delete();
          message.channel.send(args.join(" ").substring(5));
      }
      } else {
      message.channel.send("You do not have the Appropiate Permissions. `Manage_Messages`");
      }
  }
  if(commandIs("help", message)){
      message.author.send(h.help.join("\n"))
  }
  if(commandIs("ping", message)){
    if(message.author.id === owner) return message.channel.send(`🏓 Ping! Pong! | ${Math.floor(client.ping)}ms`);
    message.channel.send(`Pong! | ${Math.floor(client.ping)}ms`)
  }
  if(commandIs("avatar", message)){
      if(message.mentions.users.size < 1){
          message.channel.send(message.author.displayAvatarURL)
      } else {
          message.channel.send(message.mentions.users.first().displayAvatarURL);
      }
  }
  if(commandIs("guildavatar", message)){
      message.channel.send(message.guild.iconURL)
  }

  //Moderation Commands
  if(commandIs("info", message)){
      if(message.mentions.users.size < 1){
      const embed = new Discord.RichEmbed()
      .setTitle(`Info on ${message.author.username}#${message.author.discriminator}`)
      .setDescription('**Playing** ' + (message.author.presence.game ? message.author.presence.game.name : "None"))
      .setColor('#0000ff')
      .setFooter(`Requested by ${message.author.username} at ${message.createdAt}`)
      .setThumbnail(message.author.displayAvatarURL)
      .addField("Nickname:", message.author.nickname, true)
      .addField("User ID:", message.author.id, true)
      .addField("Avatar:", message.author.avatarURL, true)
      .addField("Status:", message.author.presence.status, false);

      message.channel.send({embed});
      } else {
      const embed = new Discord.RichEmbed()
      .setTitle(`Info on ${message.mentions.users.first().username}#${message.mentions.users.first().discriminator}`)
      .setDescription('**Playing** ' + (message.mentions.users.first().presence.game ? message.mentions.users.first().presence.game.name : "None"))
      .setColor('#0000ff')
      .setFooter(`Requested by ${message.mentions.users.first().username} at ${message.createdAt}`)
      .setThumbnail(message.mentions.users.first().displayAvatarURL)
      .addField("Nickname:", message.mentions.users.first().nickname, true)
      .addField("User ID:", message.mentions.users.first().id, true)
      .addField("Avatar:", message.mentions.users.first().avatarURL, true)
      .addField("Status:", message.mentions.users.first().presence.status, false);

      message.channel.send({embed});
      }
  }
  if(commandIs('rank', message)){
    sql.get(`SELECT * FROM scores WHERE userId = '${message.author.id}'`).then(row => {
      if (!row) return message.reply('You are Nothing to me');
      message.reply(`\nYou are level ${row.level}\nYou have ${row.points} Exp`);
    })
  }
  if(commandIs('kick', message)){
    let kickUser = message.mentions.users.first();
    if(!kickUser){
      message.channel.send("No user Specified, `Usages: $kick (Mention)`")
    } else if(message.member.hasPermissions(["KICK_MEMBERS"]) || message.author.id === owner){
      message.guild.member(kickUser).kick();
    } else {
      message.channel.send("Ya don't have the needed Perms `KICK_MEMBERS`");
    }
  }
  if(commandIs('ban', message)){
    let banUser = message.mentions.users.first();
    let days = args[2];
    if(!banUser || !days){
      message.channel.send(`Not given the correct arguments | \`$ban mention days\``)
    } else if(message.member.hasPermission("BAN_MEMBERS") || message.author.id === owner){
      const embed = new Discord.RichEmbed()
      .setAuthor(`${banUser.username}#${banUser.discriminator}`)
      .setDescription('Are you sure you want to ban this member?\nType `yes` to confirm or wait 10 seconds to deny.')
      .setFooter(`Requested by ${message.author.username}#${message.author.discriminator}`);

      message.channel.send({embed})
      .then(() => {
        message.channel.awaitMessages(response => response.content === 'yes', {
          max: 1,
          time: 10000,
          errors: ['time'],
        })
        .then(() => {
          message.channel.send(`${banUser.username}#${banUser.discriminator} has been banned from the server!`);
          message.guild.ban(banUser, days);
        })
        .catch(() => {
          message.channel.send('Command was cancelled due to no correct response.');
        })
      })
      // message.guild.ban(banUser, days)
    } else {
      message.channel.send('You do not have the needed perms `BAN_MEMBERS`')
    }
  }
  if(commandIs("stats", message)){
    var date = new Date(client.uptime);
    var days = date.getUTCDate() - 1;
    var hours = date.getUTCHours();
    var minutes = date.getUTCMinutes();
    // let uptime = moment(new Date(new Date() - client.uptime)).format("D [days], H [hrs], m [mins], s [secs]");
    const embed = new Discord.RichEmbed()
    .setAuthor(`Requested by ${message.author.username}`, message.author.displayAvatarURL, "https://discord.gg/SJd3DrT")
    .setDescription(message.author.presence.game ? message.author.presence.game.name : "Not Playing Anything")
    .addField("Guilds", client.guilds.size, false)
    .addField("Channels", client.channels.size, false)
    .addField("Emojis", client.emojis.size, false)
    .addField("Users", client.users.size, false)
    .addField("Uptime", `${days} days, ${hours} hrs, ${minutes} mins`, false);

    message.channel.send({embed});
  }
  if(commandIs("prune", message)){
    if(message.member.hasPermissions(["MANAGE_MESSAGES", "ADMINISTRATOR"]) || message.author.id === owner){
      let messagecount = parseInt(args[0]);
      if(!args[1]) return message.channel.send('Please Specify Amount!');
      message.channel.fetchMessages({limit: args[1]})
      .then(messages => message.channel.bulkDelete(messages));
      message.channel.send(args[1] + " messages has been removed from existence :thumbsup:");
    } else {
      message.channel.send("You do not have the Appropiate Perms. `Manage_Messages, Administrator`")
    }
  }
  /*
  if(commandIs("createrole", message)){
    let guild = message.guild;
    let roleName = args[1];
    let mentionHoist = args[2];
    let roleColor = args[3];
    if(!message.member.hasPermissions(["MANAGE_ROLES_OR_PERMISSIONS"])){
      message.channel.send("Ya don't have the needed Perms `MANAGE_ROLES_OR_PERMISSIONS`");
    }else if(!roleName || !roleColor || !mentionHoist){
      message.channel.send("Incorrect usage | `+createrole [rolename] [yes/no] [Hex Color Code]`");
    }else if(mentionHoist === 'no' || mentionHoist === 'No'){
      guild.createRole({name: roleName, color: roleColor, hoist: false, mentionable: false}).catch(error => message.channel.send('Incorrect usage | `+createrole [rolename] [yes/no] [Hex Color Code]`'))
      message.channel.send(`${roleName} has been created!`);
    }else{
      guild.createRole({name: roleName, color: roleColor, hoist: true, mentionable: true}).catch(error => console.log(error.stack))
      message.channel.send(`${roleName} has been created!`);
    }
  }
  if(commandIs("giverole", message)){
    let giveRole = message.mentions.users.first();
    if(!giveRole){
      return message.channel.send('Incorrect Usage | `+giverole @mention roleid`');
    }
    let role = args.join(' ').substring(args[1].length + args[0].length + 2);
    if(!message.member.hasPermissions(["MANAGE_ROLES_OR_PERMISSIONS"]) || !message.author.id !== owner){
      message.channel.send("You don't have the required Perms `MANAGE_ROLES_OR_PERMISSIONS`")
    } else if(!message.guild.roles.exists('name', role)){
      message.channel.send(`${role} Doesn't Exist`);
    }else {
      let roleId = message.guild.roles.find('name', role).id;
      message.guild.member(giveRole).addRole(roleId).catch(error => message.channel.send('Incorrect Usage | `+giverole @mention rolename`'))
      message.channel.send(`${role} has been given!`);
    }
  }
  if(commandIs("takerole", message)){
    let role = args.join(' ').substring(args[0].length + 1);
    if(!message.guild.roles.exists('name', role)){
      message.channel.send(`${role} Doesn't Exist`)
    } else if(!role){
      message.channel.send(`Incorrect Usage | \`+takerole role\``)
    } else{
      let roleId = message.guild.roles.find('name', role).id;
      if(!message.member.roles.has(roleId)){
        message.channel.send(`You do not have this role! ${role}`);
      } else{
        message.member.removeRole(roleId).catch(error => message.channel.send('Incorrect Usage | `+takerole role`'))
        message.channel.send(`${role} has been Taken Away :'(`);
      }
    }
  }
  */

  //Fun Commands
  if(commandIs("justdoit", message)){
      message.channel.send("http://i.imgur.com/VivBAZ2.png")
  }
  if(commandIs('mystery', message)){
      message.channel.send(mystery.box[Math.floor(Math.random() * mystery.box.length)])
  }
  if(commandIs('dice', message)){
    let dice = args[1];
    if(!dice){
      message.channel.send(`:8ball: | You rolled a ${Math.floor(Math.random() * 100)}`)
    } else if(dice === "∞"){
      message.channel.send(`:8ball: | You rolled a ∞`)
    }else{
      message.channel.send(`:8ball: | You rolled a ${Math.floor(Math.random() * dice)}`)
    }
  }
  if(commandIs("woah", message)){
    message.channel.send("http://i.imgur.com/sP4MMV7.gif")
  }
  if(commandIs('cookie', message)){
    if(message.mentions.users.size < 1){
      message.channel.send('Give someone a Cookie? or keep it?')
    } else {
      message.channel.send(`:cookie: | **${message.author.username}** has Given <@${message.mentions.users.first().id}> a **Cookie**`)
    }
  }
  if(commandIs('tableflip', message)){
    if(message.mentions.users.size < 1){
      message.channel.send("Mention someone to use this")
    } else{
      message.channel.send(`${message.author.username} Threw a Table at ${message.mentions.users.first().username}\n(╯°□°）╯︵ ┻━┻`)
    }
  }
  if(commandIs("awshit", message)){
    message.channel.send(image.awshit[Math.floor(Math.random() * image.awshit.length)])
  }
  if(commandIs('8ball', message)){
    let question = args[1];
    if(!question){
      message.channel.send("Ask a Question.")
    } else {
      message.channel.send(`:8ball: | ${ball[Math.floor(Math.random() * ball.length)]}, ${message.author.username}`)
    }
  }
  if(commandIs('f', message)){
    let respect = args[1];
    if(!respect){
      message.channel.send(`**${message.author.username}** paid their respects to **Lord Hackysack**`)
    } else {
      message.channel.send(`**${message.author.username}** paid their respects to **${args.join(' ').substring(3)}**`)
    }
  }
  if(commandIs('shank', message)){
    if(message.mentions.users.size < 1) return message.channel.send('Mention someone to use this');
    var target = message.mentions.users.first();
    var status = [
      'The Victim bleeds out and dies',
      'The Victim is wounded on the ground but lives',
      'The Victim is not phased and kills murderer'
    ]
    var kill = [
      `**${message.author.username}** has a Shank! 👀`,
      `(╯°□°）╯︵ ┻━┻`,
      `**${message.author.username}** creeps up on target **\`${target.username}\`**😈`,
      `/////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\`,
      `**${message.author.username}** pulls out the **shank** 🔪`,
      `and then starts repeatedly ***stabbing ${target.username}***!`
    ];
    message.channel.send(`${kill.join('\n')}\n\n${status[Math.floor(Math.random() * status.length)]}`)
  }
  if(commandIs('rate', message)){
    let rate = args.join(' ').substring(6);
    let rating = Math.floor(Math.random() * 10);
    if(!rate){
      message.channel.send("Theres nothing to rate :unamused:")
    } else if(rating < 5){
      message.channel.send(`:robot: | I rate **${rate}** a ${rating}/10 :thumbsdown:`)
    } else if(args[1].toLowerCase() === "hackysack"){
      message.channel.send(`:robot: | I rate **Myself** a solid 10/10 :confetti_ball:`)
    } else {
      message.channel.send(`:robot: | I rate **${rate}** a ${rating}/10 :thumbsup:`)
    }
  }
  if(commandIs('roasted', message)){
    message.channel.send('http://i.imgur.com/bA6FESh.gif')
  }
  if(commandIs('kys', message)){
    message.channel.send('http://ropestore.org');
  }
  if(commandIs('b1nzy', message)){
    message.channel.send('http://i.imgur.com/mNgIJ21.png')
  }
  if(commandIs('nicememe', message)){
    message.channel.send('http://niceme.me/')
  }
  if(commandIs('idiot', message)){
    message.channel.send('http://youareanidiot.org/');
  }
  if(commandIs('blockem', message)){
    message.channel.send('http://i.imgur.com/zdVe35n.png');
  }
  if(commandIs('memeshield', message)){
    message.channel.send(`${message.author}, **Our Lord and Saviour has intrusted a shield into your hands, use it wisely and protect yourself from all the normies in the world**`);
    message.channel.send('http://i.imgur.com/VvW8oij.png');
  }

  //Music Commands
/*
  if(commandIs('leavechannel', message)){
    message.member.voiceChannel.leave();
  }
  if(commandIs('airhorn', message)){
    let voiceChannel = message.member.voiceChannel;
    if(!voiceChannel) return message.channel.send('You have to be in a voice channel.');
    voiceChannel.join()
    .then(connection => {
      const dispatcher = connection.playFile('./airhorns/airhorn.mp3');
      dispatcher.on('end', () => {
        voiceChannel.leave();
      });
    });
  }
  if(commandIs('sadviolin', message)){
    let voiceChannel = message.member.voiceChannel;
    if(!voiceChannel) return message.channel.send('You have to be in a voice channel.');
    voiceChannel.join()
    .then(connection => {
      const dispatcher = connection.playFile('./airhorns/sadviolin.mp3');
      dispatcher.on('end', () => {
        voiceChannel.leave();
      });
    });
  }
*/
  //Owner Commands
  if(message.author.id !== owner){
    return;
    }else{
    if(commandIs('eval', message)){
        try {
            var code = args.join(" ").substring(6);
            var evaled = eval(code);
            if(message.content.includes('token')) return;
            if(typeof evaled !== "string")
                evaled = require("util").inspect(evaled);

            if(message.content.includes('token')) return message.channel.send("ya thought wanka");
            if(message.content.includes('2 + 2')) return message.channel.send(`:arrow_forward:**Input**\`\`\`js\n${message.content.substring(6)}\`\`\`\n:arrow_down:**Output**\`\`\`xl\n2\`\`\``)
            message.channel.send(`:arrow_forward:**Input**\`\`\`js\n${message.content.substring(6)}\`\`\`\n:arrow_down:**Output**\`\`\`xl\n${clean(evaled)}\`\`\``)
        } catch (err) {
            message.channel.send(`\`ERROR\` \`\`\`xl\n${clean(err)}\n\`\`\``);
        }
    }
    if(commandIs(('channeldelete', 'cdelete'), message)){
      message.channel.delete();
    }
    if(commandIs(('channelbirth', "cbirth"), message)){
      message.channel.send(`\`\`\`diff\n-This Channel was Created\n+${moment(message.channel.createdAt).format('MMMM Do YYYY, h:mm:ss a')}\`\`\``);
    }
    if(commandIs(('createchannel', 'ccreate'), message)){
      var cname = args[1];
      let include = message.content;
      if(!cname) return;
      if(args[2]) return message.channel.send('Bad Characters');
      message.guild.createChannel(cname.toLowerCase(), 'text');
    }
    if(commandIs(('channelname', 'cname'), message)){
      var cname = args[1];
      if(!cname) return;
      message.channel.send(`${message.channel.name} has been changed to ${cname}`);
      message.channel.setName(cname);
    }

    if(commandIs("test", message)){
      message.channel.send('This await message will be cancelled in 30 seconds if the required text is not provided')
      .then(() => {
        message.channel.awaitMessages(response => response.content === "test" || response.content === "no", {
          max: 1,
          time: 30000,
          errors: ['time'],
        })
        .then((collected) => {
          message.channel.send('The Message was a success, ' + collected.first().content)
        })
        .catch(() => {
          message.channel.send('There was no Message Provided!')
        })
      })
    }
    if(commandIs("gsay", message)){
        if(args.length === 1){
            message.channel.send("You did not define an Argument.\nUsage: `$say 'msg'`");
        } else {
            message.delete();
            message.channel.send(args.join(" ").substring(5));
        }
    }
    if(commandIs("ghelp", message)){
      help = [
        "List of Commands for Government Official!",
        "`eval` Evaluate js Code",
        "`channeldelete` delete a channel",
        "`createchannel` create a channel",
        "`channelname` name a channel",
        "`govcreaterole` create a role",
        "`govgiverole` give self role",
        "`test` Superior Test Command",
        "`gsay` Bot says what u said",
        "`ghelp` this thingy",
        "`blacklist` list of blacklisted users",
        "`msend` dms the user provided"
      ];
      message.author.send(help.join('\n'))
    }
    if(commandIs("blacklist", message)){
      const embed = new Discord.RichEmbed()
      .setTitle("List of Blacklisted Peeps")
      .setDescription(black.names.join(',\n'))

      message.channel.send({embed});
    }
    if(commandIs('msend', message)){
      let userid = args[1];
      if(!userid){
      message.reply('Incorrect Usage\n\`\`\`js\nThis is Correct Usage\n<command> <userid> <message>\`\`\`');
      } else{
        client.users.get(userid).send(args.join(' ').substring(args[0].length + args[1].length + 1))
      }
    }
    if(commandIs("playing", message)){
      var k = null
        if(args[1] === "null"){
          client.user.setGame(null);
        }
        client.user.setGame(args.join(" ").substring(9))
    }



  }//End of Owner Commands


});
client.on("messageDelete", message =>{
  let guild = message.guild;
  if(message.author.bot) return;
  const channel = guild.channels.find('name', 'mod-log');
  if(!channel) return;

  channel.send(`📝 | \`${moment().format('LTS')}\` **${message.author.username}'s** message was deleted:\n${message.cleanContent}`);
});








// Events

client.on('guildCreate', guild =>{
    guild.defaultChannel.send('Thank you for inviting **__Lord Hackysack__** to your server!\nType $help to take a look at all my bots\nHeres an invite to the HQ of our Establishment\nhttps://discord.gg/UfPT4Zp\nGovernment Official#6591 created this bot using discord.js')
    // console.log(chalk.green('NewGuildCreate') + " = " + guild.name + " (" + guild.id + ")")
    console.log(`${chalk.green(`NewGuild`)} = ${guild.name} (${client.guilds.size})`)
    client.channels.get(config.newserverlog).send(`New Guild (${client.guilds.size})\n\`Guild Name\` ${guild.name}[${guild.id}]\n\`Guild Owner\` ${guild.owner.user.username}\n\`User Amount\` ${guild.members.size}\n\`Channels\` ${guild.channels.size}\n\`Guild Icon\` ${guild.iconURL ? guild.iconURL : "null"}`);
});
client.on('guildDelete', guild =>{
    // console.log(chalk.red('GuildDeleted') + " - " + guild.name)
    console.log(`${chalk.red('LeftGuild')} - ${guild.name} (${client.guilds.size})`)
    client.channels.get(config.newserverlog).send("Left Server (" + client.guilds.array().length + ")\n**__" + guild.name + "__**");
});
client.on('unhandledRejection', err => {
    console.error("Uncaught Promise Erro: \n" + err.stack)
});













//
